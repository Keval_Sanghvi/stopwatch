const start = document.querySelector('#start');
const stop = document.querySelector('#stop');
const reset = document.querySelector('#reset');
const lap = document.querySelector('#lap');
const clear = document.querySelector('#clear-laps');
const laps = document.querySelector('#laps');
const minutes = document.querySelector('#minutes');
const seconds = document.querySelector('#seconds');
const milliseconds = document.querySelector('#milliseconds');
const para = document.querySelector('.para');

stopwatch();
function stopwatch() {
    let time;
    let mins = 0;
    let secs = 0;
    let millisecs = 0;
    
    start.addEventListener('click', function() {
        clearInterval(time);
        time = setInterval(timer, 10);
    });
    
    stop.addEventListener('click', function() {
        clearInterval(time);
    });
    
    reset.addEventListener('click', function() {
        clearInterval(time);
        mins = 0;
        secs = 0;
        millisecs = 0;
        minutes.innerHTML = '0' + mins;
        seconds.innerHTML = '0' + secs;
        milliseconds.innerHTML = '0' + millisecs;
    });
    
    lap.addEventListener('click', createItem);
    clear.addEventListener('click', clearLaps);
    
    function timer() {
        
        millisecs++;
        
        if(millisecs<9) {
            milliseconds.innerHTML = '0' + millisecs;
        }
        if(millisecs>9) {
            milliseconds.innerHTML = millisecs;
        }
        if(millisecs>99) {
            secs++;
            seconds.innerHTML = '0' + secs;
            millisecs = 0;
            milliseconds.innerHTML = '0' + millisecs;
        }
        if(secs>9) {
            seconds.innerHTML = secs;
        }
        if(secs>59) {
            mins++;
            minutes.innerHTML = '0' + mins;
            secs = 0;
            seconds.innerHTML = '0' + secs;
        }
        if(mins<9) {
            minutes.innerHTML = '0' + mins;
        }
        if(mins>9) {
            minutes.innerHTML = mins;
        }
    }
}

function createItem() {
    const li = document.createElement('li');
    li.className = 'items';
    li.appendChild(document.createTextNode(para.textContent));
    laps.appendChild(li);
}

function clearLaps() {
    while(laps.firstChild) {
        laps.removeChild(laps.firstChild);
    }
}